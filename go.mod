module gitlab.com/younixcc/filefilego

go 1.13

require (
	github.com/blevesearch/bleve v1.0.5
	github.com/boltdb/bolt v1.3.1
	github.com/cznic/b v0.0.0-20181122101859-a26611c4d92d // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/cznic/strutil v0.0.0-20181122101858-275e90344537 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/ethereum/go-ethereum v1.9.12
	github.com/facebookgo/ensure v0.0.0-20200202191622-63f1cf65ac4c // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20200203212716-c811ad88dec4 // indirect
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.3.5
	github.com/hako/durafmt v0.0.0-20191009132224-3f39dc1ed9f4
	github.com/jmhodges/levigo v1.0.0 // indirect
	github.com/libp2p/go-libp2p v0.7.4
	github.com/libp2p/go-libp2p-connmgr v0.2.1
	github.com/libp2p/go-libp2p-core v0.5.1
	github.com/libp2p/go-libp2p-discovery v0.3.0
	github.com/libp2p/go-libp2p-kad-dht v0.6.2
	github.com/libp2p/go-libp2p-mplex v0.2.3
	github.com/libp2p/go-libp2p-pubsub v0.2.6
	github.com/libp2p/go-libp2p-secio v0.2.2
	github.com/libp2p/go-libp2p-tls v0.1.3
	github.com/libp2p/go-libp2p-yamux v0.2.7
	github.com/multiformats/go-multiaddr v0.2.1
	github.com/naoina/toml v0.1.2-0.20170918210437-9fafd6967416
	github.com/pborman/uuid v1.2.0
	github.com/peterh/liner v1.2.0
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.2.0
	github.com/tecbot/gorocksdb v0.0.0-20191217155057-f0fad39f321c // indirect
	github.com/urfave/cli v1.22.4
	golang.org/x/crypto v0.0.0-20200311171314-f7b00557c8c4
)
